-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2021 at 07:43 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teltics`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_menu_detail`
--

CREATE TABLE `m_menu_detail` (
  `m_menu_detail_id` int(20) NOT NULL,
  `m_menu_group_id` int(20) NOT NULL,
  `m_menu_sub_id` int(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `route_name` varchar(100) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_menu_detail`
--

INSERT INTO `m_menu_detail` (`m_menu_detail_id`, `m_menu_group_id`, `m_menu_sub_id`, `nama`, `route_name`, `active`) VALUES
(1, 1, 1, 'User', 'user.list', 1),
(2, 2, 2, 'Product', 'product.list', 1),
(3, 2, 3, 'Services', 'service.list', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_menu_group`
--

CREATE TABLE `m_menu_group` (
  `m_menu_group_id` int(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `fa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_menu_group`
--

INSERT INTO `m_menu_group` (`m_menu_group_id`, `nama`, `active`, `fa`) VALUES
(1, 'Master', 1, 'fa-database'),
(2, 'Website', 1, 'fa-desktop');

-- --------------------------------------------------------

--
-- Table structure for table `m_menu_sub`
--

CREATE TABLE `m_menu_sub` (
  `m_menu_sub_id` int(20) NOT NULL,
  `m_menu_group_id` int(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `fa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_menu_sub`
--

INSERT INTO `m_menu_sub` (`m_menu_sub_id`, `m_menu_group_id`, `nama`, `active`, `fa`) VALUES
(1, 1, 'User Management', 1, 'fa-users'),
(2, 2, 'Product', 1, 'fa-desktop'),
(3, 2, 'Service', 1, 'fa-desktop');

-- --------------------------------------------------------

--
-- Table structure for table `m_product`
--

CREATE TABLE `m_product` (
  `m_product_id` int(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_product`
--

INSERT INTO `m_product` (`m_product_id`, `nama`, `image`, `create_date`, `update_date`, `active`) VALUES
(1, 'Bali United', 'storage/product/YtuFAu47IBjpBPUJwSPgUQYmTRxxjclUte4chzIr.jpeg', '2021-09-26 09:59:57', '2021-09-26 10:17:23', 1),
(2, 'Bali United', 'storage/product/YtuFAu47IBjpBPUJwSPgUQYmTRxxjclUte4chzIr.jpeg', '2021-09-26 09:59:57', '2021-09-26 10:17:23', 1),
(3, 'Bali United', 'storage/product/YtuFAu47IBjpBPUJwSPgUQYmTRxxjclUte4chzIr.jpeg', '2021-09-26 09:59:57', '2021-09-26 10:17:23', 1),
(4, 'Bali United', 'storage/product/YtuFAu47IBjpBPUJwSPgUQYmTRxxjclUte4chzIr.jpeg', '2021-09-26 09:59:57', '2021-09-26 10:17:23', 1),
(5, 'Bali United', 'storage/product/YtuFAu47IBjpBPUJwSPgUQYmTRxxjclUte4chzIr.jpeg', '2021-09-26 09:59:57', '2021-09-26 10:17:23', 1),
(6, 'Bali United', 'storage/product/YtuFAu47IBjpBPUJwSPgUQYmTRxxjclUte4chzIr.jpeg', '2021-09-26 09:59:57', '2021-09-26 10:17:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_service`
--

CREATE TABLE `m_service` (
  `m_service_id` int(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_service`
--

INSERT INTO `m_service` (`m_service_id`, `nama`, `image`, `create_date`, `update_date`, `active`) VALUES
(1, 'Bali Festival', 'storage/service/vBaKRwbWGNXEI5wRkeAro06OIDhLsZJxT2pbMSbF.png', '2021-09-26 10:28:39', '2021-09-26 10:28:39', 1),
(2, 'Bali Festival', 'storage/service/vBaKRwbWGNXEI5wRkeAro06OIDhLsZJxT2pbMSbF.png', '2021-09-26 10:28:39', '2021-09-26 10:28:39', 1),
(3, 'Bali Festival', 'storage/service/vBaKRwbWGNXEI5wRkeAro06OIDhLsZJxT2pbMSbF.png', '2021-09-26 10:28:39', '2021-09-26 10:28:39', 1),
(4, 'Bali Festival', 'storage/service/vBaKRwbWGNXEI5wRkeAro06OIDhLsZJxT2pbMSbF.png', '2021-09-26 10:28:39', '2021-09-26 10:28:39', 1),
(5, 'Bali Festival', 'storage/service/vBaKRwbWGNXEI5wRkeAro06OIDhLsZJxT2pbMSbF.png', '2021-09-26 10:28:39', '2021-09-26 10:28:39', 1),
(6, 'Bali Festival', 'storage/service/vBaKRwbWGNXEI5wRkeAro06OIDhLsZJxT2pbMSbF.png', '2021-09-26 10:28:39', '2021-09-26 10:28:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `active`) VALUES
(1, 'Creative', 'creative@gmail.com', '$2y$10$u6pyb1TzqTLFdyQtyj9R5u/4IuAWQ7qLx2Hskc0z7rg319BQgXIzK', 'BdvhNZ3rFqyAByCfOGj9CO2YD3MwYJt0wwUZWVbQ4BniTCa2kS3eKsoFr2P2', '2021-09-26 07:29:12', '2021-09-26 07:29:12', 1),
(12, 'Tes', 'tes', '$2y$10$253vCJOnxpfmsLFHqjsLuuBtNNzOWW4ccJjtCMG4fdgr3reIJ6mx6', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_menu`
--

CREATE TABLE `users_menu` (
  `users_menu_id` int(20) NOT NULL,
  `users_id` int(20) NOT NULL,
  `m_menu_detail_id` int(20) NOT NULL,
  `_read` int(1) NOT NULL,
  `_add` int(1) NOT NULL,
  `_update` int(1) NOT NULL,
  `_delete` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_menu`
--

INSERT INTO `users_menu` (`users_menu_id`, `users_id`, `m_menu_detail_id`, `_read`, `_add`, `_update`, `_delete`) VALUES
(3, 12, 1, 1, 1, 1, 1),
(4, 1, 1, 1, 1, 1, 1),
(5, 1, 2, 1, 1, 1, 1),
(6, 1, 3, 1, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_menu_detail`
--
ALTER TABLE `m_menu_detail`
  ADD PRIMARY KEY (`m_menu_detail_id`);

--
-- Indexes for table `m_menu_group`
--
ALTER TABLE `m_menu_group`
  ADD PRIMARY KEY (`m_menu_group_id`);

--
-- Indexes for table `m_menu_sub`
--
ALTER TABLE `m_menu_sub`
  ADD PRIMARY KEY (`m_menu_sub_id`);

--
-- Indexes for table `m_product`
--
ALTER TABLE `m_product`
  ADD PRIMARY KEY (`m_product_id`);

--
-- Indexes for table `m_service`
--
ALTER TABLE `m_service`
  ADD PRIMARY KEY (`m_service_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_menu`
--
ALTER TABLE `users_menu`
  ADD PRIMARY KEY (`users_menu_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_menu_detail`
--
ALTER TABLE `m_menu_detail`
  MODIFY `m_menu_detail_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_menu_group`
--
ALTER TABLE `m_menu_group`
  MODIFY `m_menu_group_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_menu_sub`
--
ALTER TABLE `m_menu_sub`
  MODIFY `m_menu_sub_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_product`
--
ALTER TABLE `m_product`
  MODIFY `m_product_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `m_service`
--
ALTER TABLE `m_service`
  MODIFY `m_service_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users_menu`
--
ALTER TABLE `users_menu`
  MODIFY `users_menu_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
