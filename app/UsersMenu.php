<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UsersMenu extends Model
{
    protected $table = 'users_menu';
      protected $primaryKey="users_menu_id";
    protected $fillable=["users_id", "m_menu_detail_id","_read", "_add", "_update", "_delete"];
    const CREATED_AT = "create_date";
    const UPDATED_AT = "update_date";

    public static function getPermission($id_menu){
        $user=Auth::user()->id;
        $permission=UsersMenu::where("users_id",$user)
            ->where("m_menu_detail_id",$id_menu)
            ->get(["_read", "_add", "_update", "_delete"]);
        return $permission;
    }
}
