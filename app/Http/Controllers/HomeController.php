<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use \App\Product;
use \App\Service;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('adminlte.home');
    }

      public function homes()
    {

        $data_product = Product::where('active', 1)->get();
          $data_service = Service::where('active', 1)->get();
        return view('frontend.pages.home',['data_product' => $data_product,'data_service' => $data_service]);
    }
}
