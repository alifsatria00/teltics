<?php

namespace App\Http\Controllers;

use App\Product;
use App\UsersMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $permission=UsersMenu::getPermission(2);
        if($permission[0]->_read==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            return view("adminlte.website.w_product.index", compact("permission"));
        }

    }

    public function data(){
        $product=Product::get_list_product();
        $permission=UsersMenu::getPermission(2);
        return DataTables::of($product)
            ->addColumn("img_product", function ($product){
                return "<img src='".asset($product->image)."' style='height:50px'/>";
            })
            ->addColumn("action", function ($product){
                $permission=UsersMenu::getPermission(2);
                $editable="";
                $deletable="";
                //style='visibility: hidden'
                if($permission[0]->_update==0){
                    $editable="disabled='disabled' onclick='return false;'";
                }
                if($permission[0]->_delete==0){
                    $deletable="disabled='disabled' onclick='return false;'";
                }
                return "<a href='".route("product.edit", base64_encode($product->m_product_id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit' ".$editable."><span class='fa fa-edit'/></a> <a href=\"javascript:showConfirm('".base64_encode($product->m_product_id)."')\"  class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='bottom' title='Delete' ".$deletable."><span class='fa fa-trash'/></a> ";
            })
            ->rawColumns(["img_product", "action"])
            ->make(true);
    }

    public function add(){
        $permission=UsersMenu::getPermission(2);
        if($permission[0]->_add==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            return view("adminlte.website.w_product._form", compact("permission"));
        }
    }

    public function save(Request $request){
        $this->validate($request,[
            "nama"=>"required",
            "image"=>"required|image|mimes:jpeg,png,bmp",
        ],[
            "nama.required"=>"Nama Tidak Boleh Kosong",
            "image.required"=>"Image Tidak Boleh Kosong",
            "image.image"=>"Image Harus Berupa Image",
            "image.mimes"=>"Image Harus Berupa jpeg,png,bmp",
        ]);

        try{
            $image=$request->file("image");
            $newName = Storage::disk("Http")->put("product", $image);

            $product=new Product();
            $product->nama=ucwords($request->get("nama"));
            $product->image="storage/".$newName;
            $product->save();
            return redirect()->route("product.list");
        }
        catch (\Exception $e){
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

    public function edit($id){
        $permission=UsersMenu::getPermission(2);
        if($permission[0]->_update==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            $product=Product::findOrFail(base64_decode($id));
            $action="Edit";
           
            return view("adminlte.website.w_product._form", compact("permission", "action", "product"));
        }
    }

    public function update(Request $request){
        $this->validate($request,[
           "nama"=>"required"
,        ],[
            "nama.required"=>"Nama Tidak Boleh Kosong",
   
        ]);
        try{
            $product=Product::findOrFail($request->get("m_product_id"));
            $arrData=array();
            if($request->has("image")){
                $oldImage=str_replace("storage/","",$product->image);
                Storage::disk("Http")->delete($oldImage);
                $image=$request->file("image");
                $newName = Storage::disk("Http")->put("product", $image);
                $arrData["image"]="storage/".$newName;
            }
            $arrData["nama"]=$request->get("nama");

            $product->update($arrData);
          return redirect()->route("product.list");

        }
        catch (\Exception $e){
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

    public function delete(Request $request){
        $permission=UsersMenu::getPermission(2);
        if($permission[0]->_delete==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            try{
                $product=Product::findOrFail(base64_decode($request->get("tmpId")));
                $product->update(array("active"=>0));
                return redirect()->route("product.list");
            }
            catch (\Exception $e){
                $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
                $title="Error";
                return view("adminlte.error_page",compact("message", "title"));
            }
        }
    }
}