<?php

namespace App\Http\Controllers;

use App\Service;
use App\UsersMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $permission=UsersMenu::getPermission(3);
        if($permission[0]->_read==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            return view("adminlte.website.w_service.index", compact("permission"));
        }

    }

    public function data(){
        $service=Service::get_list_service();
        $permission=UsersMenu::getPermission(3);
        return DataTables::of($service)
            ->addColumn("img_service", function ($service){
                return "<img src='".asset($service->image)."' style='height:50px'/>";
            })
            ->addColumn("action", function ($service){
                $permission=UsersMenu::getPermission(3);
                $editable="";
                $deletable="";
                //style='visibility: hidden'
                if($permission[0]->_update==0){
                    $editable="disabled='disabled' onclick='return false;'";
                }
                if($permission[0]->_delete==0){
                    $deletable="disabled='disabled' onclick='return false;'";
                }
                return "<a href='".route("service.edit", base64_encode($service->m_service_id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit' ".$editable."><span class='fa fa-edit'/></a> <a href=\"javascript:showConfirm('".base64_encode($service->m_service_id)."')\"  class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='bottom' title='Delete' ".$deletable."><span class='fa fa-trash'/></a> ";
            })
            ->rawColumns(["img_service", "action"])
            ->make(true);
    }

    public function add(){
        $permission=UsersMenu::getPermission(3);
        if($permission[0]->_add==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            return view("adminlte.website.w_service._form", compact("permission"));
        }
    }

    public function save(Request $request){
        $this->validate($request,[
            "nama"=>"required",
            "image"=>"required|image|mimes:jpeg,png,bmp",
        ],[
            "nama.required"=>"Nama Tidak Boleh Kosong",
            "image.required"=>"Image Tidak Boleh Kosong",
            "image.image"=>"Image Harus Berupa Image",
            "image.mimes"=>"Image Harus Berupa jpeg,png,bmp",
        ]);

        try{
            $image=$request->file("image");
            $newName = Storage::disk("Http")->put("service", $image);

            $service=new Service();
            $service->nama=ucwords($request->get("nama"));
            $service->image="storage/".$newName;
            $service->save();
            return redirect()->route("service.list");
        }
        catch (\Exception $e){
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

    public function edit($id){
        $permission=UsersMenu::getPermission(3);
        if($permission[0]->_update==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            $service=Service::findOrFail(base64_decode($id));
            $action="Edit";
           
            return view("adminlte.website.w_service._form", compact("permission", "action", "service"));
        }
    }

    public function update(Request $request){
        $this->validate($request,[
           "nama"=>"required"
,        ],[
            "nama.required"=>"Nama Tidak Boleh Kosong",
   
        ]);
        try{
            $service=Service::findOrFail($request->get("m_service_id"));
            $arrData=array();
            if($request->has("image")){
                $oldImage=str_replace("storage/","",$service->image);
                Storage::disk("Http")->delete($oldImage);
                $image=$request->file("image");
                $newName = Storage::disk("Http")->put("service", $image);
                $arrData["image"]="storage/".$newName;
            }
            $arrData["nama"]=$request->get("nama");

            $service->update($arrData);
          return redirect()->route("service.list");

        }
        catch (\Exception $e){
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

    public function delete(Request $request){
        $permission=UsersMenu::getPermission(3);
        if($permission[0]->_delete==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            try{
                $service=Service::findOrFail(base64_decode($request->get("tmpId")));
                $service->update(array("active"=>0));
                return redirect()->route("service.list");
            }
            catch (\Exception $e){
                $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
                $title="Error";
                return view("adminlte.error_page",compact("message", "title"));
            }
        }
    }
}