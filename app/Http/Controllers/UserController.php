<?php

namespace App\Http\Controllers;


use App\User;
use App\UsersMenu;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $permission=UsersMenu::getPermission(1);
        if($permission[0]->_read==0){
            $title="Permission";
            $message="You Dont Have Permission To Open This Page";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            //return view("adminlte.error_page");
            return view("adminlte.master.m_user.index", compact("permission"));
        }
    }

    public function data(){
        //$user=User::where("active",1)->get();
        $user=User::getUser();
        $permission=UsersMenu::getPermission(1);
        return DataTables::of($user)
            ->addColumn("action", function($user) use ($permission){
                $editable="";
                $deletable="";
                //style='visibility: hidden'
                if($permission[0]->_update==0){
                    $editable="disabled='disabled' onclick='return false;'";
                }
                if($permission[0]->_delete==0){
                    $deletable="disabled='disabled' onclick='return false;'";
                }
                return "<a href='".route("user.edit", base64_encode($user->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit' ".$editable."><span class='fa fa-edit'/></a> <a href=\"javascript:showConfirm('".base64_encode($user->id)."')\"  class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='bottom' title='Delete' ".$deletable."><span class='fa fa-trash'/></a> ";
            })
            ->rawColumns(["action"])
            ->make(true);
    }

    public function add(){
        $permission=UsersMenu::getPermission(1);
        if($permission[0]->_add==0){
            $title="Permission";
            $message="You Dont Have Permission To Open This Page";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            return view("adminlte.master.m_user._form");
        }
    }

    public function menu($id){
        $conn=DB::connection();
            $sqlMenu="SELECT m_menu_detail.m_menu_detail_id,
                m_menu_detail.m_menu_group_id,
                m_menu_group.nama AS _group,
                m_menu_detail.m_menu_sub_id,
                m_menu_sub.nama AS _sub,
                m_menu_detail.nama,
                COALESCE(users_menu._read,0) AS _read,
                COALESCE(users_menu._add,0) AS _add,
                COALESCE(users_menu._update,0) AS _update,
                COALESCE(users_menu._delete,0) AS _delete
                FROM m_menu_detail
                LEFT JOIN m_menu_group ON m_menu_group.m_menu_group_id=m_menu_detail.m_menu_group_id AND m_menu_group.active=1
                LEFT JOIN m_menu_sub ON m_menu_sub.m_menu_sub_id=m_menu_detail.m_menu_sub_id AND m_menu_sub.active=1
                LEFT JOIN users_menu ON users_menu.m_menu_detail_id=m_menu_detail.m_menu_detail_id AND users_menu.users_id=".$id."
                WHERE m_menu_detail.active=1
                ORDER BY m_menu_group.nama ASC, m_menu_sub.nama ASC, m_menu_detail.nama ASC";
        
        $dataMenu=$conn->select($sqlMenu);

        return DataTables::of($dataMenu)
            ->addColumn("_read", function ($dataMenu){
                $checked="";
                if($dataMenu->_read==1){
                    $checked="checked";
                }
                return "<input type='checkbox' name='_read".$dataMenu->m_menu_detail_id."' value='1' ".$checked.">";
            })
            ->addColumn("_add", function($dataMenu){
                $checked="";
                if($dataMenu->_add==1){
                    $checked="checked";
                }
                return "<input type='checkbox' name='_add".$dataMenu->m_menu_detail_id."' value='1' ".$checked.">";
            })
            ->addColumn("_update", function($dataMenu){
                $checked="";
                if($dataMenu->_update==1){
                    $checked="checked";
                }
                return "<input type='checkbox' name='_update".$dataMenu->m_menu_detail_id."' value='1' ".$checked.">";
            })
            ->addColumn("_delete", function($dataMenu){
                $checked="";
                if($dataMenu->_delete==1){
                    $checked="checked";
                }
                return "<input type='checkbox' name='_delete".$dataMenu->m_menu_detail_id."' value='1' ".$checked.">";
            })
            ->rawColumns(["_read","_add","_update","_delete"])
            ->make(true);
    }

    public function save(Request $request){
        $this->validate($request, [
            "email"=>"required",
            "name"=>"required",
            "password"=>"required",
            "password"=>"required|string|confirmed",
            "password_confirmation"=>"required"
        ],[
            "email.required"=>"Email Tidak Boleh Kosong",
            "name.required"=>"Nama Tidak Boleh Kosong",
            "password.required"=>"Password Tidak Boleh Kosong",
            "password.confirmed"=>"Konfirmasi Password Tidak Boleh Kosong",
            "password_confirmation.required"=>"Password Tidak Sama"
        ]);

        $conn=DB::connection();
        try{
            $conn->beginTransaction();
            $sqlSimpan="INSERT INTO users (name, email, password, active) VALUES (
                        '".ucwords($request->get("name"))."',
                        '".$request->get("email")."',
                        '".bcrypt($request->get("password"))."',
                        1
                        )";
            $conn->statement($sqlSimpan);

            $sqlUser="SELECT id AS user_id FROM users WHERE email='".$request->get("email")."'";
            $datauser=$conn->select($sqlUser);

            $sqlMenu="SELECT m_menu_detail_id FROM m_menu_detail WHERE active=1";
            $dataMenu=$conn->select($sqlMenu);
            foreach ($dataMenu AS $dm){
                $_read=0;
                $_add=0;
                $_update=0;
                $_delete=0;
                $nmRead="_read".$dm->m_menu_detail_id;
                $nmAdd="_add".$dm->m_menu_detail_id;
                $nmUpdate="_update".$dm->m_menu_detail_id;
                $nmDelete="_delete".$dm->m_menu_detail_id;
                if($request->get($nmRead)!=null){
                    $_read=1;
                }
                if($request->get($nmAdd)!=null){
                    $_add=1;
                }
                if($request->get($nmUpdate)!=null){
                    $_update=1;
                }
                if($request->get($nmDelete)!=null){
                    $_delete=1;
                }
                $sqlSimpan="INSERT INTO users_menu (users_id, m_menu_detail_id, _read, _add, _update, _delete) VALUES (
                            ".$datauser[0]->user_id.",
                            ".$dm->m_menu_detail_id.",
                            ".$_read.",
                            ".$_add.",
                            ".$_update.",
                            ".$_delete."
                            )";
                $conn->statement($sqlSimpan);
            }
            $conn->commit();
            return redirect()->route("user.list");
        }
        catch (\Exception $e){
            $conn->rollback();
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
            //echo $e->getMessage();
        }
    }

    public function edit($id){
        $permission=UsersMenu::getPermission(1);
        if($permission[0]->_update==0){
            $title="Permission";
            $message="You Dont Have Permission To Open This Page";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            $idUser=base64_decode($id);
            $action="Edit";
            $user=User::findOrFail($idUser);
           
            return view("adminlte.master.m_user._form", compact("action", "user"));
        }
    }
    

    public function update(Request $request){
        $this->validate($request, [
            "email"=>"required",
            "name"=>"required"
        ],[
            "email.required"=>"Email Can't Empty",
            "name.required"=>"Name Can't Empty"
        ]);

        $conn=DB::connection();
        try{
            $conn->beginTransaction();
            $sqlPassword="";
            if(trim($request->get("password"))!=""){
                $sqlPassword=", password='".bcrypt($request->get("password"))."' ";
            }


            $sqlUbah="UPDATE users SET name='".ucwords($request->get("name"))."', 
                        email='".$request->get("email")."'
                        ".$sqlPassword."
                        WHERE id=".$request->get("id_user");
            $conn->statement($sqlUbah);

            $sqlHapus="DELETE FROM users_menu WHERE users_id=".$request->get("id_user");
            $conn->statement($sqlHapus);

            $sqlMenu="SELECT m_menu_detail_id FROM m_menu_detail WHERE active=1";
            $dataMenu=$conn->select($sqlMenu);
            foreach ($dataMenu AS $dm){
                $_read=0;
                $_add=0;
                $_update=0;
                $_delete=0;
                $nmRead="_read".$dm->m_menu_detail_id;
                $nmAdd="_add".$dm->m_menu_detail_id;
                $nmUpdate="_update".$dm->m_menu_detail_id;
                $nmDelete="_delete".$dm->m_menu_detail_id;
                if($request->get($nmRead)!=null){
                    $_read=1;
                }
                if($request->get($nmAdd)!=null){
                    $_add=1;
                }
                if($request->get($nmUpdate)!=null){
                    $_update=1;
                }
                if($request->get($nmDelete)!=null){
                    $_delete=1;
                }
                $sqlSimpan="INSERT INTO users_menu (users_id, m_menu_detail_id, _read, _add, _update, _delete) VALUES (
                            ".$request->get("id_user").",
                            ".$dm->m_menu_detail_id.",
                            ".$_read.",
                            ".$_add.",
                            ".$_update.",
                            ".$_delete."
                            )";
                $conn->statement($sqlSimpan);
            }
            $conn->commit();
            return redirect()->route("user.list");
        }
        catch (\Exception $e){
            $conn->rollback();
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="error";
            return view("adminlte.error_page",compact("message", "title"));
            //echo $e->getMessage();
        }
    }

    public function delete(Request $request){
        $id=base64_decode($request->get("tmpId"));
        try{
            $user = user::findOrFail($id);
            $user->update(['active'=>0]) ;
            return redirect()->route("user.list");
        }
        catch (\Exception $e){
            $title="Error";
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            return view("adminlte.error_page",compact("message", "title"));
        }
    }
}
