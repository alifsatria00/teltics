<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
class Service extends Model
{
    protected $table="m_service";
    protected $primaryKey="m_service_id";
    protected $fillable=["nama","image", "active", "create_date", "update_date"];
    const CREATED_AT = "create_date";
    const UPDATED_AT = "update_date";

public static function get_list_service(){
     $user=User::findOrFail(Auth::user()->id);

        $sql="SELECT *
				FROM m_service
                WHERE active=1
                GROUP by(m_service_id)
                ";
        $data=DB::connection()->select($sql);
        return $data;
    }
    
}
