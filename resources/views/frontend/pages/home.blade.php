<!doctype html>
<html lang="zxx"><head>
@extends('frontend.layout.head')
    
        <!-- TITLE -->
        <title>Creative United</title>
    </head>

    <body>
  
    <!--Navbar Area -->
    <div class="eorik-nav-style fixed-top">
      <div class="navbar-area">
        <!-- Menu For Mobile Device -->
        <div class="mobile-nav">
          <a href="index-2.html" class="logo">
            <img src="assets/img/mobile-manu-logo.png" alt="Logo">
          </a>
        </div>
        <!-- Menu For Desktop Device -->
        <div class="main-nav">
          <nav class="navbar navbar-expand-md navbar-light">
            <div class="container">
              <a class="navbar-brand" href="/">
                <img src="assets/img/nav/logo.png" alt="Logo" style="width: 12%;">
              </a>
              <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
         
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
    <!-- End Navbar Area -->

    

    <!-- Landing -->
    <section class="eorik-slider-area">
      <div class="eorik-slider owl-carousel owl-theme">
        <div class="eorik-slider-item slider-item-bg-1" style="background-image: url(assets/img/landing/landing1.png) !important">
          <div class="d-table">
            <div class="d-table-cell">
              <div class="container">
                <div class="eorik-slider-text one ">
                  <h1 style="text-align: left;line-height: 80px;"><i>20 MILLION <br>COMMUNITY</i></h1>
                  <div class="slider-btn">
                    <a class="default-btn" href="#" style="float: left;margin-top: 35px;    padding: 20px;">
                      LETS WORK WITH US
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
             <div class="eorik-slider-item slider-item-bg-2" style="background-image: url(assets/img/landing/landing1.png) !important">
          <div class="d-table">
            <div class="d-table-cell">
              <div class="container">
                <div class="eorik-slider-text one ">
                  <h1 style="text-align: left;line-height: 80px;">20 MILLION <br>COMMUNITY</h1>
                  <div class="slider-btn">
                    <a class="default-btn" href="#" style="float: left;margin-top: 35px;    padding: 20px;">
                      LETS WORK WITH US
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
             <div class="eorik-slider-item slider-item-bg-3" style="background-image: url(assets/img/landing/landing1.png) !important">
          <div class="d-table">
            <div class="d-table-cell">
              <div class="container">
                <div class="eorik-slider-text one ">
                  <h1 style="text-align: left;line-height: 80px;">20 MILLION <br>COMMUNITY</h1>
                  <div class="slider-btn">
                    <a class="default-btn" href="#" style="float: left;margin-top: 35px;    padding: 20px;">
                      LETS WORK WITH US
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Landing-->



    <!-- About United Creative -->
    <section class="explore-area pb-100" style="padding-top: 30px;    padding-bottom: 20px;">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-12">
        <div class="single-privacy">
          <p>Bali United Football Club is an Indonesian professional club based in Gianyar, Bali. Bali United began operations in 2014 and continue to be of the highest tier in the Indonesian football competition, League 1. The club has a vision to grow the football industry in Indonesia throught creating an acocystem cosisting of 4Cs namely the Club, Community, Corporation and Country. <span style="color: #ca171e;font-weight: bold;">Staying true to this vision</span>,the football club launched a sport agency unser the name United Creative, to always bring the <span style="color: #ca171e;font-weight: bold;">GAME ON</span> beyond its own club</p><br><br>
          <p>Warm regards,</p>
           <img src="assets/img/logo/ttd.png" alt="Image" style="    margin-left: -10%;">
        </div>
          </div>
        </div>
        <hr size="10">
      </div>
       </section>
        <!-- End About United Creative -->

      <!-- Product -->

    <section class="exclusive-area  pb-70" style="padding-bottom: 30px;"> 
      <div class="container">
        <div class="section-title" style="margin: 0;text-align-last:left;text-align:left">

          <h2 style="font-size: 40px;margin-bottom: 0px;">Product </h2>
          
           <h3 style="font-size: 24px;color: #ca171e;">What we can do for you </h3>
        </div>
             <a href="" style="float:right;padding: 10px;border: 2px solid #ca171e;color: #ca171e;margin-top: -6%;">VIEW ALL</a>
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="exclusive-wrap">
      <div class="row">
        @foreach($data_product as $dp)
          <div class="col-lg-4 col-sm-6">
            <div class="single-contact-info" style="padding: 0px;">
                <img src="{{ $dp->image}}" alt="Image" style="    width: 350px;
    height: 260px;">
              <div class="exclusive-content" style="margin-top: -50px;">
                <span class="title" style="color: #ffffff">{{ $dp->nama}}</span>
               
              </div>
            </div>
          </div>
          @endforeach
        </div>
            </div>
          </div>
        </div>
        <hr>
        <hr>
      </div>
    </section>
    <!-- End Product -->

 <!-- What Make Us -->
    <section class="city-view-area">
      <div class="container">
        <div class="section-title" style="margin: 0;text-align-last:left;text-align:left;    max-width: fit-content;">

          <h2 style="font-size: 64px;margin-bottom: 0px;"><b>WHAT MAKE US DIFFERENT</b> </h2>
        </div>
          <div class="single-city-item owl-carousel owl-theme">
            <div class="city-view-single-item">
              <div class="city-content">
                 <div class="row align-items-center">
          <div class="col-lg-6">
          <img src="assets/img/produk/what.png" alt="Image" style="width: 100%;
    height: 300px;">
    <div class="centered" style=" position: absolute;
  top: 50%;
  left: 60%;    width: fit-content;
  transform: translate(-50%, -50%);"><h2 style="font-size: 80px;color: #ffffff;">10.000</h2><h3 style="font-size: 30px;color: #ffffff;line-height: 0px;">COMMUNITY LEADER</h3></div>
          </div>
            <div class="col-lg-6">
               <div class="customer-area">
      <div class="container-fluid p-0">
     
     <h3 style="font-size:20px">Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups</h3>
      </div>
    </div>
          </div>
        </div>
              </div>
            </div>
            <div class="city-view-single-item">
              <div class="city-content">
                 <div class="row align-items-center">
          <div class="col-lg-6">
          <img src="assets/img/produk/what.png" alt="Image" style="width: 100%;
    height: 300px;">
       <div class="centered" style=" position: absolute;
  top: 50%;
  left: 60%;    width: fit-content;
  transform: translate(-50%, -50%);"><h2 style="font-size: 80px;color: #ffffff;">10.000</h2><h3 style="font-size: 30px;color: #ffffff;line-height: 0px;">COMMUNITY LEADER</h3></div>
          </div>
            <div class="col-lg-6">
               <div class="customer-area ">
      <div class="container-fluid p-0">
       
        <h3 style="font-size:20px">Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups</h3>
      </div>
    </div>
          </div>
        </div>
              </div>
            </div>
          </div>
          <hr>
      </div>
    </section>
    <!-- End What Make Us -->


         <!-- Service -->
    <section class="exclusive-area  pb-70" style="padding-top: 30px"> 
      <div class="container">
        <div class="section-title" style="margin: 0;text-align-last:left;text-align:left">

          <h2 style="font-size: 40px;margin-bottom: 0px;">Services </h2>
          
           <h3 style="font-size: 24px;color: #ca171e;">What we can do for you </h3>
        </div>
             <a href="" style="float:right;padding: 10px;border: 2px solid #ca171e;color: #ca171e;margin-top: -6%;">VIEW ALL</a>
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="exclusive-wrap">
      <div class="row">
          @foreach($data_service as $ds)
          <div class="col-lg-4 col-sm-6">
            <div class="single-contact-info" style="padding: 0px;">
                <img src="{{ $ds->image}}" alt="Image" style="    width: 350px;
    height: 260px;">
              <div class="exclusive-content" style="margin-top: -50px;">
                <span class="title" style="color: #ffffff">{{ $ds->image}}</span>
               
              </div>
            </div>
          </div>
          @endforeach
        </div>
            </div>
          </div>
        </div>
        <hr>
        <hr>
      </div>
    </section>
    <!-- End Service -->

<!-- Our Work -->
    <section class="city-view-area">
      <div class="container">
        <div class="section-title" style="margin: 0;text-align-last:left;text-align:left;    max-width: fit-content;">

          <h2 style="font-size: 64px;margin-bottom: 0px;"><b>OUR WORKS</b> </h2>
        </div>
          <div class="single-city-item owl-carousel owl-theme">
            <div class="city-view-single-item">
              <div class="city-content">
                 <div class="row align-items-center">
          <div class="col-lg-4">
          <img src="assets/img/produk/work.png" alt="Image" style="width: 100%;
    height: 300px;">
          </div>
              <div class="col-lg-4">
          <img src="assets/img/produk/work.png" alt="Image" style="width: 100%;
    height: 300px;">
          </div>
          <div class="col-lg-4">
          <img src="assets/img/produk/work.png" alt="Image" style="width: 100%;
    height: 300px;">
          </div>

        </div></div></div>
                <div class="city-content">
                 <div class="row align-items-center">
          <div class="col-lg-4">
          <img src="assets/img/produk/work.png" alt="Image" style="width: 100%;
    height: 300px;">
          </div>
              <div class="col-lg-4">
          <img src="assets/img/produk/work.png" alt="Image" style="width: 100%;
    height: 300px;">
          </div>
          <div class="col-lg-4">
          <img src="assets/img/produk/work.png" alt="Image" style="width: 100%;
    height: 300px;">
          </div>

        </div></div></div>
         <hr style="argin-top: 4rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 4px solid rgba(0,0,0,.1);">
          </div>
         
      </div>
    </section>
    <!-- End Our Work -->


         <!-- Client -->
    <section class="exclusive-area  pb-70" style="padding-top: 30px"> 
      <div class="container">
        <div class="section-title" style="margin: 0;text-align-last:left;text-align:left">

          <h2 style="font-size: 40px;margin-bottom: 0px;">Clients </h2>
          
           <h3 style="font-size: 24px;color: #ca171e;">Our happy clients </h3>
        </div>
             <a href="" style="float:right;padding: 10px;border: 2px solid #ca171e;color: #ca171e;margin-top: -6%;">VIEW ALL</a>
     <div class="city-view-area">
      <div class="container">
          <div class="single-city-item owl-carousel owl-theme">
            <div class="city-view-single-item">
              <div class="city-content">
                 <div class="row align-items-center">
          <div class="col-lg-2">
          <img src="assets/img/produk/1.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
              <div class="col-lg-2">
          <img src="assets/img/produk/2.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
          <div class="col-lg-2">
          <img src="assets/img/produk/3.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
            <div class="col-lg-2">
          <img src="assets/img/produk/4.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
              <div class="col-lg-2">
          <img src="assets/img/produk/5.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
          <div class="col-lg-2">
          <img src="assets/img/produk/6.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
            <div class="col-lg-2">
          <img src="assets/img/produk/1.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
                 <div class="col-lg-2">
          <img src="assets/img/produk/3.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
              <div class="col-lg-2">
          <img src="assets/img/produk/3.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
          <div class="col-lg-2">
          <img src="assets/img/produk/4.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
            <div class="col-lg-2">
          <img src="assets/img/produk/5.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
              <div class="col-lg-2">
          <img src="assets/img/produk/6.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
          <div class="col-lg-2">
          <img src="assets/img/produk/1.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
            <div class="col-lg-2">
          <img src="assets/img/produk/2.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
                  <div class="col-lg-2">
          <img src="assets/img/produk/3.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
              <div class="col-lg-2">
          <img src="assets/img/produk/4.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
          <div class="col-lg-2">
          <img src="assets/img/produk/5.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
            <div class="col-lg-2">
          <img src="assets/img/produk/6.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>

        </div></div></div>
          <div class="city-view-single-item">
                  <div class="city-content">
                 <div class="row align-items-center">
                <div class="col-lg-2">
          <img src="assets/img/produk/1.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
              <div class="col-lg-2">
          <img src="assets/img/produk/2.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
          <div class="col-lg-2">
          <img src="assets/img/produk/3.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
            <div class="col-lg-2">
          <img src="assets/img/produk/4.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
              <div class="col-lg-2">
          <img src="assets/img/produk/5.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
          <div class="col-lg-2">
          <img src="assets/img/produk/6.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
            <div class="col-lg-2">
          <img src="assets/img/produk/1.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
                 <div class="col-lg-2">
          <img src="assets/img/produk/3.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
              <div class="col-lg-2">
          <img src="assets/img/produk/3.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
          <div class="col-lg-2">
          <img src="assets/img/produk/4.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
            <div class="col-lg-2">
          <img src="assets/img/produk/5.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
              <div class="col-lg-2">
          <img src="assets/img/produk/6.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
          <div class="col-lg-2">
          <img src="assets/img/produk/1.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
            <div class="col-lg-2">
          <img src="assets/img/produk/2.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
                  <div class="col-lg-2">
          <img src="assets/img/produk/3.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
              <div class="col-lg-2">
          <img src="assets/img/produk/4.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
          <div class="col-lg-2">
          <img src="assets/img/produk/5.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>
            <div class="col-lg-2">
          <img src="assets/img/produk/6.png" alt="Image" style="width: 100%;
    height: 90px;">
          </div>

        </div></div></div></div>
          </div>
      </div>
    </div>
      </div>
    </section>
    <!-- End Client -->
<style type="text/css">
  .yellow-orange {
   background-image: linear-gradient(90deg, #dae601 0%, #dae601 7%, #e2cb07 7%, #e2cb07 14%, #eab00d 14%, #eab00d 21%, #f29412 21%, #f29412 28%, #fa7918 28%, #fa7918 100%);
   background-size: 100% 18%;
}
 
h1 {
   margin-bottom: 3.5rem;
}
 h5 span {
   background-size: 100% 15%;
   background-repeat: repeat-x;
   background-position: left 0% bottom 10%;
}

</style>
      <section class="exclusive-area  pb-70" style="padding-bottom: 30px;"> 
      <div class="container">
        <div class="section-title" style="margin: 0;text-align-last:center;text-align:center;    max-width: 100%;">
           <h5 style="font-size: 40px;color: black; font-family: 'HK-Grotesk';"><i>LET'S CREATE</i> <span class="yellow-orange" style="display: inline;font-size: 42px;font-weight: bolder;color: black; font-family: 'HK-Grotesk';letter-spacing: inherit;"><i>CREATIVE PROJECT</i></span> </h5>
        </div>
      </div>
    </section>

   
@extends('frontend.layout.footer')
@extends('frontend.layout.script')
    </body>
</html>