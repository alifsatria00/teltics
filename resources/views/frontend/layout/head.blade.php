
<style>
  @font-face /*perintah untuk memanggil font eksternal*/
  {
    font-family: 'hk-grotesk'; /*memberikan nama bebas untuk font*/
    src: url('assets/fonts/hk/HKGrotesk-Bold.woff');/*memanggil file font eksternalnya di folder nexa*/
  }
 
  h2
  {
    font-family: 'hk-grotesk';
  }
</style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS --> 
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
        <!-- Beautiful Fonts CSS --> 
        <link rel="stylesheet" href="{{asset('assets/css/beautiful-fonts.css')}}">
        <!-- Owl Theme Default CSS --> 
        <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.min.css')}}">
        <!-- Owl Carousel CSS --> 
        <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
        <!-- Owl Magnific CSS --> 
        <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}">
        <!-- Animate CSS --> 
        <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">
        <!-- Boxicons CSS --> 
       <link rel="stylesheet" href="{{asset('assets/css/boxicons.min.css')}}"> 
        <!-- Flaticon CSS --> 
         <link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
        <!-- Meanmenu CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/meanmenu.css')}}">
        <!-- Nice Select CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/nice-select.css')}}">
       <!-- Date Picker CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/date-picker.css')}}">
       <!-- Odometer CSS-->
       <link rel="stylesheet" href="{{asset('assets/css/odometer.css')}}">
        <!-- Style CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
        <!-- Responsive CSS -->
       <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{asset('assets/img/favicon.png')}}">