
   <footer class="footer-top-area footer-top-area-two ">
      <div class="container">
       <div class="footer-middle-area pt-60">
          <div class="row">
            <div class="col-lg-3 col-md-6">
              <div class="single-widget">
                <a href="/">
                   <img src="assets/img/nav/logo.png" alt="Logo" style="width: 50%;">
                </a>
                <h2 style="font-size:16px;color:white">UNITRD CREATIVE HEAD OFFICE</h2>
                   <h2 style="font-size:13px;color:white">Wisma Bali United, Jalan Panjang No.29<br>Kedoya Selatan, Jakarta Barat<br>(021) 12345678</h2>
                
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-6">
              <div class="single-widget" style="text-align: center;">
                <h2 style="font-size:13px;color:white;padding-bottom: 10px;">Product</h2>
                 <h2 style="font-size:13px;color:white;padding-bottom: 10px;">Services</h2>
                  <h2 style="font-size:13px;color:white;padding-bottom: 10px;">Works</h2>
                   <h2 style="font-size:13px;color:white">Client</h2>
        
              </div>
            </div>
             <div class="col-lg-3 col-md-6">
              <div class="single-widget" style="text-align: center;">
                <h2 style="font-size:13px;color:white;padding-bottom: 10px;">About Us</h2>
                 <h2 style="font-size:13px;color:white;padding-bottom: 10px;">Contact Us</h2>
                  <h2 style="font-size:13px;color:white;padding-bottom: 10px;">Career</h2>
        
              </div>
            </div>
            <div class="col-lg-3 col-md-6">
              <div class="instagram-link" style="text-align:right;">
              <a href="#" style="padding-bottom: 10px;">
                <i class="bx bxl-instagram" style="    font-size: 40px;
    content: '\e92f';
    background: white;
    padding: 3px;
    border-radius: 15px;
    color: black;"></i>
              </a>
                <h2 style="font-size:13px;color:white;padding-bottom: 10px;">COPYRIGHT &copy; 2021</h2>
            </div>
            </div>
          </div>
        </div>
      </div>
    </footer>