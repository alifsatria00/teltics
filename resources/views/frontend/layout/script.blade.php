  
        <!-- Jquery Slim JS -->
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
        <!-- Popper JS -->
        <script src="{{asset('assets/js/popper.min.js')}}"></script>
        <!-- Bootstrap JS -->
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <!-- Meanmenu JS -->
    <script src="{{asset('assets/js/jquery.meanmenu.js')}}"></script>
        <!-- Wow JS -->
        <script src="{{asset('assets/js/wow.min.js')}}"></script>
        <!-- Owl Carousel JS -->
    <script src="{{asset('assets/js/owl.carousel.js')}}"></script>
        <!-- Owl Magnific JS -->
    <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
        <!-- Nice Select JS -->
    <script src="{{asset('assets/js/jquery.nice-select.min.js')}}"></script>
    <!-- Appear JS --> 
        <script src="{{asset('assets/js/jquery.appear.js')}}"></script>
    <!-- Odometer JS --> 
    <script src="{{asset('assets/js/odometer.min.js')}}"></script>
    <!-- Bootstrap Datepicker JS -->
    <script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- Parallax JS --> 
    <script src="{{asset('assets/js/parallax.min.js')}}"></script>
    <!-- Mixitup JS -->
    <script src="{{asset('assets/js/jquery.mixitup.min.js')}}"></script>
    <!-- Form Ajaxchimp JS -->
    <script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>
    <!-- Form Validator JS -->
    <script src="{{asset('assets/js/form-validator.min.js')}}"></script>
    <!-- Contact JS -->
    <script src="{{asset('assets/js/contact-form-script.js')}}"></script>
    <!-- Map JS FILE -->
        <script src="{{asset('assets/js/map.js')}}"></script>
        <!-- Custom JS -->
    <script src="{{asset('assets/js/custom.js')}}"></script>