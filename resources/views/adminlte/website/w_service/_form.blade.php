@extends('adminlte.layout.app')

@section("style")
@endsection

@section("content")
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Service
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route("home") !!}"><i class="fa fa-dashboard"></i> Website</a></li>
            <li class="active"><a href="{!! route("service.list") !!}"> Service</a></li>
            <li class="active"> Add</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-title"></div>
                        <div class="pull-right box-tools">
                            <a href="{!! route("service.list") !!}" class="btn btn-danger btn-sm" data-toggle="tooltip"
                               title="Cancel">
                                <i class="fa fa-close"></i>
                            </a>
                            <a href="javascript:$('#form-user').submit()" class="btn btn-success btn-sm"
                               data-toggle="tooltip" title="{!! isset($action)?$action:"Save" !!}">
                                <i class="fa {!! isset($action)?"fa-edit":"fa-save" !!}"></i>
                            </a>
                        </div>
                    </div>

                    <div class="box-body pad">
                        {!! Form::open(['url' => isset($action)?route("service.update"):route("service.save"), 'method' => 'post', 'id'=>'form-user', "enctype"=>"multipart/form-data"]) !!}
                        {!! Form::hidden("m_service_id", isset($service)?$service->m_service_id:null) !!}
                        <div class="row">
                              <div class="col-md-6 col-12">
                                <div class="form-group {!! $errors->has("image")?"has-error":"" !!}">
                                    {!! Form::label("image", "Image service *") !!}
                                    {!! Form::file("image", ["class"=>"form-control", "required"]) !!}
                                    @if(isset($service))
                                        <img src="{!! asset($service->image) !!}" style="height: 50px">
                                    @endif
                                    {!! $errors->first("image", "<p class='help-block'>:message</p>") !!}
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group {!! $errors->has("nama")?"has-error":"" !!}">
                                    {!! Form::label("nama", "Nama *") !!}
                                    {!! Form::text("nama", isset($service)?$service->nama:null, ["class"=>"form-control", "style"=>"text-transform: capitalize", "required"]) !!}
                                    {!! $errors->first("nama", "<p class='help-block'>:message</p>") !!}
                                </div>
                            </div>
                           
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection

@section("script")
@endsection