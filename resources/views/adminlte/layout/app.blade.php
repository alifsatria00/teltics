<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include("adminlte.layout.head")
    <!-- Styles -->
    @yield('style')
</head>
<!--
untuk sidebar default tertutup tambahkan class = "sidebar-collapse" di <body>
-->
<body class="hold-transition skin-blue sidebar-mini">

<header class="main-header">
    @include("adminlte.layout.navbar")
</header>

@include("adminlte.layout.sidebar")

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="height: 100%">
    @yield("content")
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
    @include("adminlte.layout.footer")
</footer>

@include("adminlte.layout.script")
@yield("script")
</body>
</html>