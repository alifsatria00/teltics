<div class="pull-right hidden-xs">
    <b>Version</b> {!! env("APP_VERSION") !!}
</div>
<strong>Copyright &copy; 2021 Teltics Media</strong>