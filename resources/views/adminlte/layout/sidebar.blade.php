<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!--
        <div class="user-panel">
            <div class="pull-left image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        -->
        <!-- search form -->
        <!--
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <?php
                $user=\Illuminate\Foundation\Auth\User::find(\Illuminate\Support\Facades\Auth::user()->id);
                $conn=\Illuminate\Support\Facades\DB::connection();
                $sqlGrup="SELECT m_menu_detail.m_menu_group_id,
                            m_menu_group.nama AS grup,
                            m_menu_group.fa
                            FROM users_menu
                            INNER JOIN m_menu_detail ON m_menu_detail.m_menu_detail_id=users_menu.m_menu_detail_id
                            INNER JOIN m_menu_group ON m_menu_group.m_menu_group_id=m_menu_detail.m_menu_group_id
                            WHERE users_menu.users_id=".$user->id."
                            AND users_menu._read=1
                            GROUP BY m_menu_detail.m_menu_group_id, m_menu_group.nama, m_menu_group.fa
                            ORDER BY m_menu_group.nama";
                $dataGrup=$conn->select($sqlGrup);

                $sqlSub="SELECT m_menu_detail.m_menu_group_id,
                        m_menu_detail.m_menu_sub_id,
                        m_menu_sub.nama AS sub,
                        m_menu_sub.fa
                        FROM users_menu
                        INNER JOIN m_menu_detail ON m_menu_detail.m_menu_detail_id=users_menu.m_menu_detail_id AND m_menu_detail.m_menu_sub_id IS NOT NULL
                        LEFT JOIN m_menu_sub ON m_menu_sub.m_menu_sub_id=m_menu_detail.m_menu_sub_id
                        WHERE users_menu.users_id=".$user->id."
                        AND users_menu._read=1
                        GROUP BY m_menu_detail.m_menu_group_id,
                        m_menu_detail.m_menu_sub_id,
                        m_menu_sub.nama,
                        m_menu_sub.fa
                        ORDER BY m_menu_sub.nama";
                $dataSub=$conn->select($sqlSub);

                $sqlMenu="SELECT m_menu_detail.m_menu_group_id,
                        m_menu_group.nama AS grup,
                        m_menu_detail.m_menu_sub_id,
                        m_menu_sub.nama AS sub,
                        m_menu_detail.m_menu_detail_id,
                        m_menu_detail.nama AS menu,
                        m_menu_detail.route_name
                        FROM users_menu
                        INNER JOIN m_menu_detail ON m_menu_detail.m_menu_detail_id=users_menu.m_menu_detail_id AND m_menu_detail.active=1
                        INNER JOIN m_menu_group ON m_menu_group.m_menu_group_id=m_menu_detail.m_menu_group_id AND m_menu_group.active=1
                        LEFT JOIN m_menu_sub ON m_menu_sub.m_menu_sub_id=m_menu_detail.m_menu_sub_id AND m_menu_sub.active=1
                        WHERE users_menu.users_id=".$user->id."
                        AND users_menu._read=1
                        ORDER BY m_menu_group.nama,
                        m_menu_sub.nama,
                        m_menu_detail.nama";
                $dataMenu=$conn->select($sqlMenu);

                foreach ($dataGrup AS $dg){
                    //class="active treeview menu-open"
                    $active="";
                    $open="";
                    if(Request::segment(1)=='master' && $dg->m_menu_group_id==1){
                        $active="active";
                        $open="menu-open";
                    }
                     if(Request::segment(1)=='Website' && $dg->m_menu_group_id==2){
                        $active="active";
                        $open="menu-open";
                    }
                   
                    ?>
                    <li class="{!! $active !!} treeview {!! $open !!}">
                        <a href="#">
                            <i class="fa {!! $dg->fa !!}"></i> <span>{!! $dg->grup !!}</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <?php
                            foreach ($dataSub AS $ds){
                                if($ds->m_menu_group_id==$dg->m_menu_group_id){
                                    $activeSub="";
                                    $openSub="";
                                    if(Request::segment(2)=="user" && $ds->m_menu_sub_id==1){
                                        $activeSub="active";
                                        $openSub="menu-open";
                                    }
                                        if(Request::segment(2)=="product" && $ds->m_menu_sub_id==2){
                                        $activeSub="active";
                                        $openSub="menu-open";
                                    }
                                        if(Request::segment(2)=="service" && $ds->m_menu_sub_id==3){
                                        $activeSub="active";
                                        $openSub="menu-open";
                                    }
                                   
                                    ?>
                                    <li class="{!! $activeSub !!} treeview {!! $openSub !!}">
                                        <a href="#"><i class="fa {!! $ds->fa !!}"></i> {!! $ds->sub !!}
                                            <span class="pull-right-container">
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                        </a>
                                        <ul class="treeview-menu">
                                            <?php
                                            foreach ($dataMenu AS $dm){
                                                if($dm->m_menu_group_id==$dg->m_menu_group_id && $dm->m_menu_sub_id==$ds->m_menu_sub_id){
                                                    $activeMenu="";
                                                    if(Request::segment(2)=="user" && $dm->m_menu_detail_id==1){
                                                        $activeMenu="active";
                                                    }
                                                    if(Request::segment(2)=="product" && $dm->m_menu_detail_id==2){
                                                        $activeMenu="active";
                                                    }
                                                    if(Request::segment(2)=="service" && $dm->m_menu_detail_id==3){
                                                        $activeMenu="active";
                                                    }
                                                    
                                                    
                                                    ?>
                                                    <li class="{!! $activeMenu !!}"><a href="{!! route($dm->route_name) !!}"><i class="fa fa-circle-o"></i> {!! $dm->menu !!}</a></li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                    <?php
                                }
                            }
                            foreach ($dataMenu AS $dm){
                                if($dm->m_menu_group_id==$dg->m_menu_group_id && is_null($dm->m_menu_sub_id)){
                                    $activeMenu="";
                                    if(Request::segment(2)=="Transaksi" && $dm->m_menu_detail_id==3){
                                        $activeMenu="active";
                                    }

                                   
                                    ?>
                                    <li class="{!! $activeMenu !!}"><a href="{!! route($dm->route_name) !!}"><i class="fa fa-circle-o"></i> {!! $dm->menu !!}</a></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                    <?php
                }
            ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                            <p>Will be 23 on April 24th</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-user bg-yellow"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                            <p>New phone +1(800)555-1234</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                            <p>nora@example.com</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-file-code-o bg-green"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                            <p>Execution time 5 seconds</p>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Custom Template Design
                            <span class="label label-danger pull-right">70%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Update Resume
                            <span class="label label-success pull-right">95%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Laravel Integration
                            <span class="label label-warning pull-right">50%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Back End Framework
                            <span class="label label-primary pull-right">68%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->

        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
                <h3 class="control-sidebar-heading">General Settings</h3>

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Report panel usage
                        <input type="checkbox" class="pull-right" checked>
                    </label>

                    <p>
                        Some information about this general settings option
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Allow mail redirect
                        <input type="checkbox" class="pull-right" checked>
                    </label>

                    <p>
                        Other sets of options are available
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Expose author name in posts
                        <input type="checkbox" class="pull-right" checked>
                    </label>

                    <p>
                        Allow the user to show his name in blog posts
                    </p>
                </div>
                <!-- /.form-group -->

                <h3 class="control-sidebar-heading">Chat Settings</h3>

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Show me as online
                        <input type="checkbox" class="pull-right" checked>
                    </label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Turn off notifications
                        <input type="checkbox" class="pull-right">
                    </label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Delete chat history
                        <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                    </label>
                </div>
                <!-- /.form-group -->
            </form>
        </div>
        <!-- /.tab-pane -->
    </div>
</aside>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->