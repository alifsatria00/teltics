@extends('adminlte.layout.app')

@section("style")
@endsection

@section("content")
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route("home") !!}"><i class="fa fa-dashboard"></i> Master</a></li>
            <li>User Management</li>
            <li class="active"><a href="{!! route("user.list") !!}"> User</a> </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-title"></div>
                        @if(isset($permission) && $permission[0]->_add==1)
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <a href="{!! route("user.add") !!}" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="bottom" title="Add Data"><i class="fa fa-plus"></i></a>
                            </div>
                            <!-- /. tools -->
                        @endif
                    </div>

                    <div class="box-body pad">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Email</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Do You Want To Delete This Data?.</p>
                        {!! Form::open(['url' => route("user.delete"), 'method' => 'post', "enctype"=>"multipart/form-data", 'id'=>"form-hapus"]) !!}
                    {!! Form::hidden("tmpId","",["id"=>"tmpId"]) !!}
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-info" data-dismiss="modal" onclick="$('#form-hapus').submit();">Ok</button>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("script")
    <script>
        $(function () {
            $('#example1').DataTable({
                serverSide: true,
                processing: true,
                ajax: "{{route('user.data')}}",
                columns: [
                    {data:'email'},
                    {data:'name'},
                    {data: 'action', orderable: false, searchable: false}
                ],
                order:[[0,'asc']],
                pageLength:10,
                lengthMenu:[10,50,100],
            });
        });
        function showConfirm(id) {
            $("#tmpId").val(id);
            $("#myModal").modal("show");
        }
    </script>
@endsection