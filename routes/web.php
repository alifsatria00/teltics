<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@homes')->name('homes');

Auth::routes();
// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->get('logout', 'Auth\LoginController@logout')->name('auth.logout');
Auth::routes();
// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(["prefix"=>"master"], function (){
    Route::group(["prefix"=>"user"], function (){
        Route::get("list", "UserController@index")->name("user.list");
        Route::get("data", "UserController@data")->name("user.data");
        Route::get("add", "UserController@add")->name("user.add");
        Route::get("menu/{id}", "UserController@menu")->name("user.menu");
        Route::post("save", "UserController@save")->name("user.save");
        Route::get("edit/{id}", "UserController@edit")->name("user.edit");
        Route::post("update", "UserController@update")->name("user.update");
        Route::post("delete", "UserController@delete")->name("user.delete");
    });
     });

     Route::group(["prefix"=>"website"], function (){
        Route::group(["prefix"=>"product"], function (){
        Route::get("list", "ProductController@index")->name("product.list");
        Route::get("data", "ProductController@data")->name("product.data");
        Route::get("add", "ProductController@add")->name("product.add");
        Route::get("menu/{id}", "ProductController@menu")->name("product.menu");
        Route::post("save", "ProductController@save")->name("product.save");
        Route::get("edit/{id}", "ProductController@edit")->name("product.edit");
        Route::post("update", "ProductController@update")->name("product.update");
        Route::post("delete", "ProductController@delete")->name("product.delete");
    });
        Route::group(["prefix"=>"service"], function (){
        Route::get("list", "ServiceController@index")->name("service.list");
        Route::get("data", "ServiceController@data")->name("service.data");
        Route::get("add", "ServiceController@add")->name("service.add");
        Route::get("menu/{id}", "ServiceController@menu")->name("service.menu");
        Route::post("save", "ServiceController@save")->name("service.save");
        Route::get("edit/{id}", "ServiceController@edit")->name("service.edit");
        Route::post("update", "ServiceController@update")->name("service.update");
        Route::post("delete", "ServiceController@delete")->name("service.delete");
    });
        });
